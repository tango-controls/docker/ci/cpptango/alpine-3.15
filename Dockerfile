FROM alpine:3.19

# alpine creates group named like the user for us
ARG APP_UID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake
ENV PKG_CONFIG_PATH=/home/tango/lib/pkgconfig

RUN apk add --no-cache    \
  bash                    \
  bzip2                   \
  cmake                   \
  curl                    \
  curl-dev                \
  c-ares-dev              \
  g++                     \
  git                     \
  jpeg-dev                \
  libzmq                  \
  make                    \
  openssl-dev             \
  patch                   \
  python3                 \
  python3-dev             \
  re2-dev                 \
  sudo                    \
  zeromq-dev              \
  zlib-dev                \
  zstd-dev &&             \
  rm -rf /var/cache/apk/*

RUN adduser -D -u "$APP_UID" -s /bin/bash tango                                    && \
    addgroup tango wheel                                                           && \
    echo "%wheel ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers                           && \
    echo "/lib:/usr/local/lib:/usr/lib:/home/tango/lib" > /etc/ld-musl-x86_64.path

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_UID" scripts/*.sh ./
COPY --chown="$APP_UID":"$APP_GID" scripts/patches/* ./patches/

RUN ./common_setup.sh               && \
    ./install_cppzmq.sh             && \
    ./install_omniorb.sh            && \
    ./install_tango_idl.sh          && \
    ./install_catch.sh              && \
    ./install_opentelemetry_deps.sh && \
    ./install_opentelemetry.sh      && \
    rm -rf dependencies
